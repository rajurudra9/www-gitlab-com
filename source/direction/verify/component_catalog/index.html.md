---
layout: markdown_page
title: "Category Direction - Component Catalog"
description: "Reusable pipeline configuration and definitions" 
canonical_path: "/direction/verify/component_catalog/"
---

- TOC
{:toc}

## Our vision
Foster a collaborative community of developers who can easily share, build, and maintain high-quality CI/CD configurations. By providing a platform that embraces the open-source process, we aim to empower developers to focus on true innovation and unlock the full potential of the open-source ecosystem. Through our commitment to inclusivity, transparency, and accessibility, we strive to create a culture of continuous learning and improvement, where every community member can contribute.

## Our Mission
Provide the best-in-class experience for building and maintaining CI/CD pipelines.

## Glossary
This section defines the terminology throughout this project. With these terms we are only identifying abstract concepts and are subject to changes as we refine the design by discovering new insights.

* **Component** The reusable unit of pipeline configuration.
* **Project** The GitLab project attached to a repository. A project can contain multiple components.
* **Catalog** The collection of projects that are set to contain components, which will be broken down into two flavors:
  * **Private catalog** Scoped per namespace
  * **Public catalog** Single source of truth
* **Version** The release name of a tag in the project, which allows components to be pinned to a specific revision.

### Components catalog
The component catalog will be a collection of components which:

* Is the SSOT for users to browse and search for the pipeline components they desire.
* Contains shareable and reusable packages of YAML configurations (components) that could be attached to any CI configuration.
* Has an easy way to document usage of each component.
* Allows users to contribute to the catalog by developing and publishing their components.

### Pipeline component
A pipeline component is a reusable single-purpose building block that abstracts away a single pipeline configuration unit. Components are used to compose a part or entire pipeline configuration. It can optionally take input parameters and set output data to be adaptable and reusable in different pipeline contexts while encapsulating and isolating implementation details.

Components allow a pipeline to be assembled by using abstractions instead of having all the details defined in one place. Therefore, when using a component in a pipeline, a user shouldn't need to know the implementation details of the component and should only rely on the provided interface.

A pipeline component defines its type which indicates in which context of the pipeline configuration the component can be used. For example, a component of type X can only be used according to the type X use-case.

For best experience with any systems made of components, it's fundamental that components are:

* Single purpose: A component must focus on a single goal and the scope must be as small as possible.
* Isolated: When a component is used in a pipeline, its implementation details should not leak outside the component itself into the main pipeline.
* Reusable: A component is designed to be used in different pipelines. Depending on the assumptions it's built on, a component can be more or less generic. Generic components are more reusable but may require more customization.
* Versioned: When using a component we must specify the version we are interested in. The version identifies the exact interface and behavior of the component.
* Resolvable: When a component depends on another component, this dependency must be explicit and trackable.

Some of those best practices will be enforced in the product while others would need to be enforced by the user.


### Roadmap
* [Pre-requisite for CI component](https://gitlab.com/groups/gitlab-org/-/epics/9530) - Implement `spec:input`, which will allow users to define an input for any includable file, by doing so we will provide our existing users that are using CI templates an immediate value by allowing them to declare define inputs to be used with any file which is included in their pipeline.
* [CI Components Requirements](https://gitlab.com/groups/gitlab-org/-/epics/9408) - Define what is a component, its structure, limitation, how to use it, and more.
* [Introduce CI component](https://gitlab.com/groups/gitlab-org/-/epics/9409) - Implement an MVC of the CI component functionality, dogfood it by converting existing templates into components, and release it as beta.
* [CI Components Catalog](https://gitlab.com/groups/gitlab-org/-/epics/9410) - Implement an MVC of a CI component which will be the user interface layer and the scaffolding for components in an organization.

## What's Next
The work for the [Pre-requisite for CI component](https://gitlab.com/groups/gitlab-org/-/epics/9530) epic is underway. In this phase we are going to implement some of the component elements into our existing templates solution so users could gain value and provide their feedback to help guide us in future iterations.

At the end of this epic users should be able to:
* Specify input parameters 
* Set a default value
* Specify mandatory & optional inputs
* Enable input interpolation

## Dogfooding
The best way to understand how GitLab works is to use it for as much of your job as possible, this is why we practice [dogfooding](https://about.gitlab.com/handbook/values/#dogfooding) We have recently begun collaborating with our internal productivity team to work on the CI catalog. This effort is being tracked through this [collaboration issue](https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/141), and the team is currently identifying commonly used CI templates and attempting to convert them into components. Our goal is to understand our customers' challenges when converting their templates into components.

## Competitive Landscape
Notable competitors in this space are:

- GitHub actions with their [actions marketplace](https://github.com/marketplace?category=&query=&type=actions&verification=)
- [Circle CI orbs](https://circleci.com/developer/orbs)
- CodeFresh also provide their users with a [CI step library](https://codefresh.io/steps/)

Watch this walkthrough video of the different contribution frameworks available by these competitors:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/7WSWGDtMD7A" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## FAQ

**Q1 - Why are you not using templates?**

* GitLab CI Templates have not been designed with reusability in mind. Within a template a user can define global keywords such as `variables:` that affects the entire pipeline leading to inheritance issues. Users should be able to convert their existing templates to CI components.

**Q2 - Why aren't you using variables?**

* Up until now, users have used environment variables to provide information to templates or downstream pipelines. However, this can be problematic because global variables can potentially affect all pipelines (and templates). The more variables you use, the more likely it is for them to have an impact on your entire pipeline. It's important to note that variable interpolation will still be supported, and users could still use variables within a component but the way to pass parameters to a component will be through a declarative input schema.
